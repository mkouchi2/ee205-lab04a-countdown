#pragma once

#include <stdio.h>
#include <time.h>
#include <unistd.h>


void initializeRefTime();
int initializeCurrentYear();
void startCountUp();
void startCountDown();


struct tm refTime;
struct tm *currentTime;
