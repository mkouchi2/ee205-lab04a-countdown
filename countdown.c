///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Matthew Kouchi <mkouchi2@hawaii.edu>
// @date   04_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#include "countdown.h"

time_t rawTime;
int year;

void intializeRefTime()
{
   // Initialize reference time data
   refTime.tm_year = 2014;
   refTime.tm_mday = 21;
   refTime.tm_hour = 4;
   refTime.tm_min = 26;   
   refTime.tm_sec = 7;
}

int initializeCurrentYear()
{
   int intermediate, x;
   intermediate = currentTime->tm_year + 1900;
   x = intermediate - 2014;
   return x;
}


void startCountUp()
{
      for ( int i=1 ; i!=0 ; i++)
      {
   // Get Current Time 
         time ( &rawTime );
         currentTime = localtime (&rawTime);
  
   // Initialize Years Passed From Reference Time
         year = initializeCurrentYear();

   // Print Countdown
          printf("Years: %d\tDays: %d\tHours: %d\tMinutes: %d\tSeconds: %d\n", year, currentTime->tm_mday, currentTime->tm_hour, currentTime->tm_min, currentTime->tm_sec);
   
   // Delay for one second
         sleep(1);
      }  
}

void startCountDown()
{
      for (int i = 1; i!=0; i++)
      {
   // Get Current Time 
         time ( &rawTime );
        
         rawTime = rawTime - (2*i);
      
         currentTime = localtime ( &rawTime ) ;
  
   // Initialize Years Passed From Reference Time
         year = initializeCurrentYear();
      
   // Print Countdown
          printf("Years: %d\tDays: %d\tHours: %d\tMinutes: %d\tSeconds: %d\n", year, currentTime->tm_mday, currentTime->tm_hour, currentTime->tm_min, currentTime->tm_sec);
   
   // Delay for one second
         sleep(1);
      }  
}


int main(int argc, char* argv[]) {
   
   // Variable Declatarion 
   char startCount;

   // Initialize reference time data
   void initializeRefTime();

   // Print Reference Time 
   printf("Reference Time: Tue Jan 21 04:26:07 PM HST 2014\n");

   // Start Countdown or Countup
   printf("Enter 'U' to countup or 'D' to countdown\n");
   scanf("%c", &startCount);
   
   // Count Up
   if (startCount == 'U')
   {
      startCountUp();
   }
   // Count Down
   else if (startCount == 'D')
   {
      startCountDown();
   }
   
   else
   {
      printf("Not Valid\n");
      return 0;
   }
   return 0;
}
