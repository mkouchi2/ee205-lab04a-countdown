###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 04a - Countdown
#
# @file    Makefile
# @version 1.0
#
# @author @todo yourName <@todo yourMail@hawaii.edu>
# @brief  Lab 04a - Countdown - EE 205 - Spr 2021
# @date   @todo dd_mmm_yyyy
###############################################################################

TARGET = countdown

CC = gcc
CFLAGS = -g

SRC = countdown.c
OBJ =$(SRC:.c=.o)
HDR =$(SRC:.c=. h)

all: $(TARGET)

$(TARGET): $(OBJ)
	$(CC) -o $(TARGET) $(OBJ) $(CFLAGS)



clean:
	rm -f $(OBJ) $(TARGET).o $(TARGET) 
